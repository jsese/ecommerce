ecommerce project
A learning project using Vue, Jquery, Bootstrap and SASS

The objective is to have a list of books to sale, distributed in 6 columns.
Each column must have 8 books and a dropdown selector to sort the books of a column
by price or by theme.
Clicking on a book a modal windows has to appear with more information about the book and a 
button to buy it. If the book is bought a counter has to be increased and shown in a 
shopping cart.

Steps:

---- June 21st ----
1- Init the project, with name ecommerce, in Kraken 
2- run in the terminal the command "compass create" in the ecommerce folder.
   The SASS files and carpets are created
3- Prepare the files:
		ecommerce/index.html
		ecommerce/js/myapp.js
		ecommerce/stylesheets/mio.css  (it is temporaly to do test, I will use 
		the file ecommerce/sass/mystyle.scss and ecommerce/stylesheets/mystyle.css
		will be automatically created)
