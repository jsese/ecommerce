// register the grid component
Vue.component('demo-grid', {
  template: '#grid-template',
  props: {
    data: Array,
    columns: Array,
    filterKey: String
  },
  data: function () {  //inicio funcion 1
    var sortOrders = {}
    this.columns.forEach(function (key) {
      sortOrders[key] = 1
    })
    return {
      sortKey: '',
      sortOrders: sortOrders
    }
  },  //cierre funcion 1
  computed: {
    filteredData: function () {  // inicio funcion 2
      var sortKey = this.sortKey
      var filterKey = this.filterKey && this.filterKey.toLowerCase()
      var order = this.sortOrders[sortKey] || 1
      var data = this.data
      if (filterKey) {
        data = data.filter(function (row) {
          return Object.keys(row).some(function (key) {
            return String(row[key]).toLowerCase().indexOf(filterKey) > -1
          })
        })
      }   // fin if (filterkey)
      if (sortKey) {
        data = data.slice().sort(function (a, b) {
          a = a[sortKey]
          b = b[sortKey]
          return (a === b ? 0 : a > b ? 1 : -1) * order
        })
      }  // fin if (sortkey)
      return data
    }  
  },  // cierre funcion 2
  filters: {
    capitalize: function (str) {
      return str.charAt(0).toUpperCase() + str.slice(1)
    }
  },
  methods: {
    sortBy: function (key) {
      this.sortKey = key
      this.sortOrders[key] = this.sortOrders[key] * -1
    }
  }
})

// bootstrap the demo
var demo = new Vue({
  el: '#demo',
  data: {
    searchQuery: '',
    gridColumns: ['columna1', 'columna2', 'columna3', 'columna4', 'columna5', 'columna6'],
    gridData: [
      { columna1: 'Chuck Norris', columna2: 'Pep Lopez',    columna3: 'Mercadona',  columna4: 'Jo que chulo', columna5: 'Mira Paki',      columna6: 'Novi Nada'},
      { columna1: 'Bruce Lee',    columna2: 'Cagon La',     columna3: 'Carrefur',   columna4: 'Tamos Todos',  columna5: 'Mira Paya',      columna6: 'Bi Rojo' },
      { columna1: 'Jackie Chan',  columna2: 'Soy Minero',   columna3: 'Caprabo',    columna4: 'Venga Ven',    columna5: 'Cons Truccion',  columna6: 'Re Paso' },
      { columna1:  'www.hp.com',  columna2: 'Paco Palotes', columna3: 'Condis',     columna4: 'Se Nada',      columna5: 'Vi Ernes',       columna6: 'No Potser'},
      { columna1: 'Jet Li',       columna2: 'Paco Palotes', columna3: 'Condis',     columna4: 'Se Nada',      columna5: 'Vi Ernes',       columna6: 'No Potser'},
      { columna1: 'Jet Li',       columna2: 'Paco Palotes', columna3: 'Condis',     columna4: 'Se Nada',      columna5: 'Vi Ernes',       columna6: 'No Potser'},
      { columna1: 'Jet Li',       columna2: 'Paco Palotes', columna3: 'Condis',     columna4: 'Se Nada',      columna5: 'Vi Ernes',       columna6: 'No Potser'},
      { columna1: 'Jet Li',       columna2: 'Paco Palotes', columna3: 'Condis',     columna4: 'Se Nada',      columna5: 'Vi Ernes',       columna6: 'No Potser'}
    ]
  }
})